//
//  MandibLever.m
//  MandibLever4
//
//  Created by Mark Westneat on 12/7/11.
//  Copyright 2011 Mark Westneat. All rights reserved.
//

#import "MandibLever.h"

@implementation MandibLever
@synthesize window;
@synthesize openButton, openCoordButton, nextJawButton, prevJawButton, saveButton, magnifyButton, reduceButton, upButton, downButton, leftButton, rightButton;
@synthesize jawrot, peakVVhill, minVVhill, mfArea, vmax, bins, opendur, pinA2, pinA3;

- (IBAction)initvars:(id)sender
{
    con = 0.017453;
    xstart = 260;
    ystart = 150;
    k = 0.25;
    scale = 220;
    fileopen=0;
    bins=20;
    newbins=bins;
    jawrot=30;
    pinA2=0;
    pinA3=0;
    molarpt=0.5;
    drawing=0;
    temprotA2=0;
    temprotA3=0;
    tempgapeA2=0;
    tempgapeA3=0;
    LJrotA2c=0;
    LJrotA3c=0;
    jawrotA2change= 0;
    jawrotA3change= 0;
    gapeA2change= 0;
    gapeA3change= 0;
    mfArea=300;
    opendur=30;
    vmax= 8;
    peakVVhill= 0.8;
    minVVhill= 0.02;
    minFFhill= (k-(k*peakVVhill))/(k+peakVVhill);
    peakFFhill= (k-(k*minVVhill))/(k+minVVhill);
}

- (IBAction)checkvars:(id)sender
{

//Check to see if variables have been edited before changing specimen or drawing- if so, change holding array
if (inlevA2 != [[inlevA2array objectAtIndex:i]doubleValue]) { 
    [inlevA2array replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:inlevA2]]; }
if (inlevA3 != [[inlevA3array objectAtIndex:i]doubleValue]) { 
    [inlevA3array replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:inlevA3]]; }
if (inlevOpen != [[inlevOpenarray objectAtIndex:i]doubleValue]) { 
    [inlevOpenarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:inlevOpen]]; }
if (outlever != [[outleverarray objectAtIndex:i]doubleValue]) { 
    [outleverarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:outlever]]; }
if (A2 != [[A2array objectAtIndex:i]doubleValue]) { 
    [A2array replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:A2]]; }
if (A3 != [[A3array objectAtIndex:i]doubleValue]) { 
    [A3array replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:A3]]; }
if (A3t != [[A3tarray objectAtIndex:i]doubleValue]) { 
    [A3tarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:A3t]]; }
if (A2j != [[A2jarray objectAtIndex:i]doubleValue]) { 
    [A2jarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:A2j]]; }
if (A3j != [[A3jarray objectAtIndex:i]doubleValue]) { 
    [A3jarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:A3j]]; }
if (A2A3ins != [[A2A3insarray objectAtIndex:i]doubleValue]) { 
    [A2A3insarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:A2A3ins]]; } 
if (ljtop != [[ljtoparray objectAtIndex:i]doubleValue]) { 
    [ljtoparray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:ljtop]]; } 
if (ljbot != [[ljbotarray objectAtIndex:i]doubleValue]) { 
    [ljbotarray replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:ljbot]]; }
if (massA2 != [[massA2array objectAtIndex:i]doubleValue]) { 
    [massA2array replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:massA2]]; } 
if (massA3 != [[massA3array objectAtIndex:i]doubleValue]) { 
    [massA3array replaceObjectAtIndex:i withObject:[NSNumber numberWithDouble:massA3]]; }
}

- (IBAction)zerovars:(id)sender
{
    distholdA2= 0;
    contdistA2=0;
    meanangvelA2=0;
    meangapevelA2=0;
    totforceA2= 0;
    totforceoutA2=0;
    tottorqueA2=0;
    meantorqueA2=0;
    meanforceA2=0;
    totworkA2=0;
    totpowerA2=0;
    totpowerA2W=0;
    meanforceoutA2=0;
    totworkA2jaw=0;
    totpowerA2jaw=0;
    totpowerA2jawW=0;
    
    distholdA3= 0;
    contdistA3=0;
    meanangvelA3=0;
    meangapevelA3=0;
    totforceA3= 0;
    totforceoutA3=0;
    tottorqueA3=0;
    meantorqueA3=0;
    meanforceA3=0;
    totworkA3=0;
    totpowerA3=0;
    totpowerA3W=0;
    meanforceoutA3=0;
    totworkA3jaw=0;
    totpowerA3jaw=0;
    totpowerA3jawW=0;
    
    A2durtot=0;
    A3durtot=0;
    temprotA2=0;
    temprotA3=0;
    tempgapeA2=0;
    tempgapeA3=0;
    LJrotA2c=0;
    LJrotA3c=0;
    biteforcehalf=0;
    biteforcebilat=0;
    maxbiteforce=0;
    LJrotA2c=0;
    LJrotA3c=0;
    jawrotA2change= 0;
    jawrotA3change= 0;
    gapecA2=0;
    gapecA3=0;
    gapeA2change= 0;
    gapeA3change= 0;
    simpercentA2=0;
    simpercentA3=0;
    
    inlevA2 = [[inlevA2array objectAtIndex:i] doubleValue];
    inlevA3 = [[inlevA3array objectAtIndex:i] doubleValue];
    inlevOpen = [[inlevOpenarray objectAtIndex:i] doubleValue]; 
    outlever = [[outleverarray objectAtIndex:i] doubleValue];
    A2 = [[A2array objectAtIndex:i] doubleValue]; 
    A3 = [[A3array objectAtIndex:i] doubleValue]; 
    A3t = [[A3tarray objectAtIndex:i] doubleValue]; 
    A2j = [[A2jarray objectAtIndex:i] doubleValue];
    A3j = [[A3jarray objectAtIndex:i] doubleValue];
    A2A3ins = [[A2A3insarray objectAtIndex:i] doubleValue]; 
    ljtop = [[ljtoparray objectAtIndex:i] doubleValue];
    ljbot = [[ljbotarray objectAtIndex:i] doubleValue]; 
    massA2 = [[massA2array objectAtIndex:i] doubleValue]; 
    massA3 = [[massA3array objectAtIndex:i] doubleValue];
}

- (IBAction)zeroarrays:(id)sender
{
[specarray insertObject:@"zero" atIndex:i]; 
[inlevA2array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[inlevA3array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[inlevOpenarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[outleverarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[A2array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[A3array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[A3tarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[A2jarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[A3jarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[A2A3insarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[ljtoparray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[ljbotarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[massA2array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
[massA3array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
    inlevA2 = [[inlevA2array objectAtIndex:i] doubleValue];
    inlevA3 = [[inlevA3array objectAtIndex:i] doubleValue];
    inlevOpen = [[inlevOpenarray objectAtIndex:i] doubleValue]; 
    outlever = [[outleverarray objectAtIndex:i] doubleValue];
    A2 = [[A2array objectAtIndex:i] doubleValue]; 
    A3 = [[A3array objectAtIndex:i] doubleValue]; 
    A3t = [[A3tarray objectAtIndex:i] doubleValue]; 
    A2j = [[A2jarray objectAtIndex:i] doubleValue];
    A3j = [[A3jarray objectAtIndex:i] doubleValue];
    A2A3ins = [[A2A3insarray objectAtIndex:i] doubleValue]; 
    ljtop = [[ljtoparray objectAtIndex:i] doubleValue];
    ljbot = [[ljbotarray objectAtIndex:i] doubleValue]; 
    massA2 = [[massA2array objectAtIndex:i] doubleValue]; 
    massA3 = [[massA3array objectAtIndex:i] doubleValue]; 
}

//---------------------------------------------------------------------------------
- (IBAction)errorcheck:(id)sender
{
        if (outlever >= (ljtop+inlevA2)) { 
            errspec = [NSString stringWithFormat:@"%d",i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: Outlever >= (jawtop + inleverA2), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (outlever >= (ljbot+inlevOpen)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: Outlever >= (ljbot + inlevOpen), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (ljtop >= (outlever+inlevA2)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: ljtop >= (outlever + inlevA2), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (ljbot >= (outlever+inlevOpen)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: ljbot >= (outlever + inlevOpen), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (A2>= (A2j+inlevA2)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: A2 >= (A2-joint + inleverA2), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (A2j>= (A2+inlevA2)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: A2-joint >= (A2 + inleverA2), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (inlevA2>= (A2+A2j)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: inleverA2 >= (A2 + A2-joint), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (A3>= (A3j+inlevA3)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: A3 >= (A3-joint + inleverA3), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (A3j>= (A3+inlevA3)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: A3-joint >= (A3 + inleverA3), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (inlevA3>= (A3+A3j)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: inleverA3 >= (A3 + A3-joint), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (A3t>= (A3)) {
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: A3-tendon should be small part of total A3, in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
             
        if (inlevA3 >= (inlevA2+A2A3ins)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: inlevA3 >= (inlevA2 + A2A3insertion distance), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (inlevA2 >= (inlevA3+A2A3ins)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: inlevA2 >= (inlevA3 + A2A3insertion distance), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
        
        if (A2A3ins >= (inlevA3+inlevA2)) { 
            errspec = [NSString stringWithFormat:@"%d", i];
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert addButtonWithTitle:@"OK"];
            [alert setMessageText:@"Trigonometry Error: A2A3insertion >= (inlevA3 + inlevA2), in specimen #"];
            [alert setInformativeText: errspec];
            [alert runModal];
            [alert release];}
}
//---------------------------------------------------------------------------------
- (IBAction)trianglecalc:(id)sender
{
    check1 = 0;
    check2 = 0;
    check3 = 0;
    
    if (sidea*sidea < (sideb*sideb + sidec*sidec))
    {   a = sideb*sideb + sidec*sidec - sidea*sidea;
        b = 2 * sideb * sidec;
        angbc = acos(a/b)/(con);
        check1 = 1;}
    
    if (sideb*sideb < (sidea*sidea + sidec*sidec))
    {   a = sidea*sidea + sidec*sidec - sideb*sideb;
        b = 2 * sidea * sidec;
        angac = acos(a/b)/(con);
        check2 = 1;}
    
    if (sidec*sidec < (sidea*sidea + sideb*sideb))
    {   a = sidea*sidea + sideb*sideb - sidec*sidec;
        b = 2 * sidea * sideb;
        angab = acos(a/b)/(con);;
        check3 = 1;}
    if (check1 == 1) {if (check2 == 1) {angab = 180 - (angbc + angac);}}
    if (check1 == 1) {if (check3 == 1) {angac = 180 - (angab + angbc);}}
    if (check2 == 1) {if (check3 == 1) {angbc = 180 - (angab + angac);}}
}
//---------------------------------------------------------------------------------
- (IBAction)processdata1:(id)sender
{ 
    minFFhill= (k-(k*peakVVhill))/(k+peakVVhill);
    peakFFhill= (k-(k*minVVhill))/(k+minVVhill);
    VVmax= peakVVhill;
    
    //CLOSED JAW GEOMETRY, CALC ANGLES BETWEEN InlevA2, A2 AND A2J: CLOSED
    sidea=inlevA2;
    sideb=A2;
    sidec=A2j;
    [self trianglecalc:nil];
    anginA2 = angab;
    anginA2j = angac;
    angA2A2j = angbc;
    
    //CALC ANGLES BETWEEN InlevA2, LJTop and OUTLEVER: CLOSED
    sidea=inlevA2;
    sideb=ljtop;
    sidec=outlever;
    [self trianglecalc:nil];
    anginA2top = angab;
    anginA2out = angac;
    angtopout = angbc;
    
    //Coords of A2 insertion: closed}
    xoA2ins= cos(anginA2out * con) * inlevA2;
    yoA2ins= sin(anginA2out * con) * inlevA2;
    
    //CALC ANGLES BETWEEN InlevA3, A3 AND A3J: CLOSED
    sidea=inlevA3;
    sideb=A3;
    sidec=A3j;
    [self trianglecalc:nil];
    anginA3 = angab;
    anginA3j = angac;
    angA3A3j = angbc;
    
    //CALC ANGLES BETWEEN InlevA3 and inlevA2: CLOSED
    sidea=inlevA3;
    sideb=inlevA2;
    sidec=A2A3ins;
    [self trianglecalc:nil];
    angA2A3 = angab;
    angA3ins = angac;
    angA2ins = angbc;
    
    //Coords of A3 insertion: closed}
    anginA3out= anginA2out-angA2A3;
    xoA3ins= cos(anginA3out * con) * inlevA3;
    yoA3ins= sin(anginA3out * con) * inlevA3;
    
    //CALC ANGLES BETWEEN InlevOpen, LJBot and OUTLEVER: CLOSED
    sidea=inlevOpen;
    sideb=ljbot;
    sidec=outlever;
    [self trianglecalc:nil];
    anginopenbot = angab;
    anginopenout = angac;
    angbotout = angbc;
    
    //COORDINATES OF DENTARY TIP and Molar Pt: CLOSED
    xod = outlever;
    yod = 0;
    xom = (xod-xoA2ins)*molarpt;
    yom = (yoA2ins-yod)*(1-molarpt);
    
    //Molar Outlever
    a = (xom*xom) + (yom*yom);
    moutlev = sqrt(a);
    
    // OPEN JAW GEOMETRY:  CALC STRETCHED A2 MUSCLE LENGTHS AND ANGLES
    anginA2jn= anginA2j+jawrot;
    a = ((A2j*A2j)+(inlevA2*inlevA2)) - (2 * A2j * inlevA2 * cos(anginA2jn * con));
    A2n = sqrt(a);
    
    //CALC ANGLES BETWEEN InlevA2, A2 AND A2J: OPEN
    sidea=inlevA2;
    sideb=A2n;
    sidec=A2j;
    [self trianglecalc:nil];
    anginA2n = angab;
    anginA2jn = angac;
    angA2A2jn = angbc;
    
    //CALC STRETCHED A3 MUSCLE LENGTHS AND ANGLES
    anginA3jn= anginA3j+jawrot;
    a = (A3j*A3j) + (inlevA3*inlevA3) - 2 * A3j * inlevA3 * cos(anginA3jn * con);
    A3n = sqrt(a);
    A3acting= A3-A3t;
    A3actingn= A3n-A3t;
    
    //CALC ANGLES BETWEEN InlevA3, A3 AND A3J: OPEN
    sidea=inlevA3;
    sideb=A3n;
    sidec=A3j;
    [self trianglecalc:nil];
    anginA3n = angab;
    anginA3jn = angac;
    angA3A3jn = angbc;
    
    //COORDINATES OF DENTARY TIP and Molar Pt: OPEN
    xnd = cos(jawrot * con) * outlever;
    ynd = sin(jawrot * con) * outlever;
    xnm = (xnd-xnA2ins)*molarpt;
    ynm = (ynA2ins-ynd)*(1-molarpt);
    
    //GAPE OPEN
    a = (xod-xnd)*(xod-xnd) + (yod-ynd)*(yod-ynd);
    gapeOpen = sqrt(a);
    
    //Coords of A2 insertion: open
    angA2insn= anginA2out-jawrot;			//new angle between coronoid process and x-axis
    xnA2ins= cos(angA2insn * con) * inlevA2;
    ynA2ins= sin(angA2insn * con) * inlevA2;
    inputdistA2= sqrt((xoA2ins - xnA2ins)*(xoA2ins - xnA2ins) + (yoA2ins - ynA2ins)*(yoA2ins - ynA2ins));
    
    //Coords of A3 insertion: open
    angA3insn= anginA3out-jawrot;			//new angle between A3inlever and x-axis}
    xnA3ins= cos(angA3insn * con) * inlevA3;
    ynA3ins= sin(angA3insn * con) * inlevA3;
    inputdistA3= sqrt((xoA3ins - xnA3ins)*(xoA3ins - xnA3ins) + (yoA3ins - ynA3ins)*(yoA3ins - ynA3ins));
    
    mAreaA2= massA2/(A2*1.05);
    mAreaA3= massA3/((A3-A3t)*1.05);
    if (pinA2>0)  
    {
        fiberlenA2= cos(pinA2*con)*A2;
        mAreaA2= massA2/(fiberlenA2*1.05);
    }
    if (pinA3>0)  
    {
        fiberlenA3= cos(pinA3*con)*(A3-A3t);
        mAreaA3= massA3/(fiberlenA3*1.05);
    }
    mforceA2=mfArea*1000*(mAreaA2/10000);
    mforceA3=mfArea*1000*(mAreaA3/10000);
}

//---------------------------------------------------------------------------------
- (IBAction)processdata2:(id)sender
{ 
    percentA2num = simpercentA2 *100; //Contract A2 Muscle to Close Jaw
    contdistA2 = A2n*simpercentA2;
    A2c= A2n-contdistA2;
    
    //CALC ANGLES BETWEEN InlevA2, A2 AND A2J: CONTRACTED
    sidea=inlevA2;
    sideb=A2c;
    sidec=A2j;
    [self trianglecalc:nil];
    anginA2c= angab;
    anginA2jc= angac;
    angA2A2jc= angbc;
    
    //DENTARY ANGLES AND COORDINATES: CONTRACTED A2
    temprotA2= LJrotA2c;				 //hold the value of rotation from last contraction: first time =0
    LJrotA2c= anginA2jn-anginA2jc;       //LJrotA2c is total closing rotation from fully open to this iteration of time bins
    jawrotA2change= LJrotA2c-temprotA2;	 //change in angle on this contraction only- LJrotA2c current - previous
    jawrotA2c= jawrot-LJrotA2c;			 //new angle of the jaw relative to coordinate system
    xndA2= cos(jawrotA2c * con) * outlever;
    if (jawrotA2c >= 0) {yndA2= sin(jawrotA2c * con) * outlever;}
    if (jawrotA2c < 0)  {yndA2= sin(jawrotA2c * con) * outlever * -1;}
    
    //GAPE CHANGE CONTRACTED A2
    tempgapeA2= gapecA2;								//hold the value of contracted gape from last contraction: first time =0
    a=(xnd-xndA2)*(xnd-xndA2)+(ynd-yndA2)*(ynd-yndA2);  //calc new contracted gape
    gapecA2 = sqrt(a);									
    gapecontA2 = -gapecA2;                      //distance moved from open
    gapeA2change= fabs(gapecA2-tempgapeA2);				//change in gape on this contraction only
    
    //Contract A3 Muscle to Close Jaw
    percentA3num= simpercentA3*100;
    contdistA3= A3actingn*simpercentA3;
    A3c= A3n-contdistA3;
    
    //CALC ANGLES BETWEEN InlevA3, A3 AND A3J: CONTRACTED}
    sidea=inlevA3;
    sideb=A3c;
    sidec=A3j;
    [self trianglecalc:nil];
    anginA3c = angab;
    anginA3jc = angac;
    angA3A3jc = angbc;
    
    //COORDINATES OF DENTARY TIP: CONTRACTED A3}
    temprotA3= LJrotA3c;
    LJrotA3c= anginA3jn-anginA3jc;
    jawrotA3change= LJrotA3c-temprotA3;
    jawrotA3c= jawrot-LJrotA3c;
    xndA3 = cos(jawrotA3c * con) * outlever;
    if (jawrotA3c >= 0) { yndA3 = sin(jawrotA3c * con) * outlever;}
    if (jawrotA3c < 0) { yndA3 = sin(jawrotA3c * con) * outlever * -1;}
    
    //GAPE CHANGE CONTRACTED A3}
    tempgapeA3= gapecA3;
    a = (xnd - xndA3)*(xnd - xndA3) + (ynd - yndA3)*(ynd - yndA3);
    gapecA3 = sqrt(a);
    gapecontA3 = gapeOpen-gapecA3;
    gapeA3change= fabs(gapecA3 - tempgapeA3);
    
    //COMPUTE MODEL OUTPUT}
    MAOpen= inlevOpen/outlever;   //jaw opening variables}
    VROpen= outlever/inlevOpen;
    angVelopen= jawrot/opendur;  //deg/ms
    velOpen= gapeOpen/opendur; //cm/ms
    
    mforceactA2= mforceA2*FFmaxA2;		//A2 acting muscle force at each bin- small at start and maximimum at end}
    A2dur= unitpercentA2/(vmax*VVmax);	//A2 duration in seconds of each bin of contraction}
    A2durms= A2dur*1000;
    MAA2= inlevA2/outlever;
    MAA2m= inlevA2/moutlev;
    VRA2= outlever/inlevA2;
    torqueA2= mforceactA2*sin(anginA2c * con)*(inlevA2/100);   //Muscle mechanics calculated at each iteration of bins}
    forceinA2= mforceactA2*sin(anginA2c * con);				
    forceoutA2= mforceactA2*sin(anginA2c * con)*MAA2;		//A2 contrib to bite force at this bin position}
    forceoutA2m= mforceactA2*sin(anginA2c * con)*MAA2m;
    maxbiteA2= mforceA2*sin(anginA2c * con)*MAA2;
    maxbiteA2m= mforceA2*sin(anginA2c * con)*MAA2m;
    emaA2= sin(anginA2c * con)*MAA2;
    angvelA2= jawrotA2change/A2durms;      // degrees per millisecond 
    gapevelA2= gapeA2change/A2durms;
    workA2= mforceactA2*((inputdistA2/bins)/100);	//A2 work (in J)calc based on force/time muscle only
    workA2mj= mforceactA2*((inputdistA2/bins)/100)*1000;	//A2 work (in J)calc based on force/time muscle only *1000 to get milliJoules
    powerA2= workA2/A2dur;                          //A2 power calc based on force/time muscle only
    powerA2W= powerA2/(massA2/1000);
    //workA2jaw= forceoutA2*(gapeA2change/100);		//A2 work, power calc based on force/time at jaw tip- should equal muscle unless friction, etc
    //powerA2jaw= workA2jaw/A2dur;
    //powerA2jawW= powerA2jaw/(massA2/1000);
    
    A2durtot= A2durtot + A2durms;			  		//Muscle mechanics summed and averaged over all bins for A2
    totforceA2= totforceA2 + mforceactA2;
    meanforceA2= totforceA2/bins;
    tottorqueA2= tottorqueA2 + torqueA2;
    meantorqueA2= tottorqueA2/bins;
    meanangvelA2= jawrot/A2durtot;
    meangapevelA2= gapeOpen/A2durtot;
    totworkA2= meanforceA2*(inputdistA2/100); //in Joules
    totworkA2mj= meanforceA2*(inputdistA2/100)*1000; //in milliJoules
    totpowerA2= totworkA2/(A2durtot/1000);
    totpowerA2W= totpowerA2/(massA2/1000);
    totforceoutA2= totforceoutA2+forceoutA2;    //Total force work and power for A2}
    meanforceoutA2= totforceoutA2/bins;
    //totworkA2jaw= meanforceoutA2*(gapeOpen/100);
    //totpowerA2jaw= totworkA2jaw/(A2durtot/1000);
    //totpowerA2jawW= totpowerA2jaw/(massA2/1000);
    
    mforceactA3= mforceA3*FFmaxA3;		//A3 acting muscle force- small at start and maximimum at end}
    A3dur= unitpercentA3/(vmax*VVmax);  //A3 duration in seconds of each bin of contraction}
    A3durms= A3dur*1000;
    MAA3= inlevA3/outlever;
    MAA3m= inlevA3/moutlev;
    VRA3= outlever/inlevA3;
    torqueA3= mforceactA3*sin(anginA3c * con)*(inlevA3/100);
    forceinA3= mforceactA3*sin(anginA3c * con);
    forceoutA3= mforceactA3*sin(anginA3c * con)*MAA3;
    forceoutA3m= mforceactA3*sin(anginA3c * con)*MAA3m;
    maxbiteA3= mforceA3*sin(anginA3c * con)*MAA3;
    maxbiteA3m= mforceA3*sin(anginA3c * con)*MAA3m;
    emaA3= sin(anginA3c * con)*MAA3;
    angvelA3= jawrotA3change/A3durms; // degrees per millisecond
    gapevelA3= gapeA3change/A3durms;
    workA3= mforceactA3*((inputdistA3/bins)/100);//in Joules
    workA3mj= mforceactA3*((inputdistA3/bins)/100)*1000;//in milliJoules
    powerA3= workA3/A3dur;
    powerA3W= powerA3/(massA3/1000);
    //workA3jaw= forceoutA3*(gapeA3change/100);
    //powerA3jaw= workA3jaw/A3dur;
    //powerA3jawW= powerA3jaw/(massA3/1000);
    
    A3durtot= A3durtot + A3durms;		//Muscle mechanics summed and averaged over all bins for A3}
    totforceA3= totforceA3 + mforceactA3;
    meanforceA3= totforceA3/bins;
    tottorqueA3= tottorqueA3 + torqueA3;
    meantorqueA3= tottorqueA3/bins;
    meanangvelA3= jawrot/A3durtot;
    meangapevelA3= gapeOpen/A3durtot;
    totworkA3= meanforceA3*(inputdistA3/100);   //Joules
    totworkA3mj= meanforceA3*(inputdistA3/100)*1000; //milliJoules
    totpowerA3= totworkA3/(A3durtot/1000);
    totpowerA3W= totpowerA3/(massA3/1000);
    totforceoutA3= totforceoutA3+forceoutA3;   //Total force work and power for A3}
    meanforceoutA3= totforceoutA3/bins;
    //totworkA3jaw= meanforceoutA3*(gapeOpen/100);
    //totpowerA3jaw= totworkA3jaw/(A3durtot/1000);
    //totpowerA3jawW= totpowerA3jaw/(massA3/1000);}
    
    biteforcehalf= forceoutA3 + forceoutA2;   //bite force on one side and both sides of the head}
    biteforcebilat= biteforcehalf*2;
    maxbiteforce= (maxbiteA3 + maxbiteA2)*2; 
    biteforcehalfm= forceoutA3m + forceoutA2m;   //bite force on one side and both sides of the head}
    biteforcebilatm= biteforcehalfm*2;
    maxbiteforcem= (maxbiteA3m + maxbiteA2m)*2; 
    
}
//---------------------------------------------------------------------------------
- (IBAction)scaledraw:(id)sender
{
    //[self checkvars:nil];
    
    //CALC VARS FOR DRAWING
    A2draw = A2;
    A3draw = A3;
    inlevA2draw = inlevA2;
    inlevA3draw = inlevA3;
    inlevOpendraw = inlevOpen;
    A2jdraw= A2j;
    A3jdraw= A3j;
    outlevdraw = outlever;
    ljtopdraw = ljtop;
    ljbotdraw = ljbot;
    A2ndraw = A2n;
    A3ndraw = A3n;
    A2cdraw = A2c;
    A3cdraw = A3c;
    
    if (outlevdraw < scale) 
    {while (outlevdraw < scale){
        A2draw = A2draw * 1.1;
        A3draw = A3draw * 1.1;
        inlevA2draw = inlevA2draw * 1.1;
        inlevA3draw = inlevA3draw * 1.1;
        inlevOpendraw = inlevOpendraw * 1.1;
        A2jdraw= A2jdraw * 1.1;
        A3jdraw= A3jdraw * 1.1;
        outlevdraw = outlevdraw * 1.1;
        ljtopdraw = ljtopdraw * 1.1;
        ljbotdraw = ljbotdraw * 1.1;
        A2ndraw = A2ndraw * 1.1;
        A3ndraw = A3ndraw * 1.1;
        A2cdraw = A2cdraw * 1.1;
        A3cdraw = A3cdraw * 1.1;}
    }
    if (outlevdraw > scale) 
    {while (outlevdraw > scale){
        A2draw = A2draw * 0.9;
        A3draw = A3draw * 0.9;
        inlevA2draw = inlevA2draw * 0.9;
        inlevA3draw = inlevA3draw * 0.9;
        inlevOpendraw = inlevOpendraw * 0.9;
        A2jdraw= A2jdraw * 0.9;
        A3jdraw= A3jdraw * 0.9;
        outlevdraw = outlevdraw * 0.9;
        ljtopdraw = ljtopdraw * 0.9;
        ljbotdraw = ljbotdraw * 0.9;
        A2ndraw = A2ndraw * 0.9;
        A3ndraw = A3ndraw * 0.9;
        A2cdraw = A2cdraw * 0.9;
        A3cdraw = A3cdraw * 0.9;}
    }
}      

//---------------------------------------------------------------------------------
- (IBAction)drawcoords:(id)sender
{ 
    //**************************************Jaw Closed
    //Quad joint and Jaw tip}  
    xQ = xstart;
    yQ = ystart;
    pt[0] = NSMakePoint(xQ,yQ);
    intxD=0;
    while (intxD <= outlevdraw) {intxD= intxD+1;}
    xD = xQ + intxD;
    yD = yQ;
    pt[1] = NSMakePoint(xD,yD);
    
    //A2 insertion: Coronoid Coords}
    intxC=0;
    while (intxC <= fabs(cos(anginA2out * con)) * inlevA2draw) {intxC= intxC+1;}
    if (anginA2out < 90) { xC = xQ + intxC;}
    if (anginA2out > 90) { xC = xQ - intxC;}
    if (anginA2out == 90) { xC = xQ;}
    intyC=0;
    while (intyC <= fabs(sin(anginA2out * con)) * inlevA2draw) {intyC= intyC+1;}
    yC = yQ + intyC;
    pt[2] = NSMakePoint(xC,yC);
    
    //Opening Inlever Coords}
    intxA=0;
    while (intxA <= fabs(cos(anginopenout * con)) * inlevOpendraw) {intxA= intxA+1;}
    if (anginopenout < 90) { xA = xQ + intxA;}
    if (anginopenout > 90) { xA = xQ - intxA;}
    if (anginopenout == 90) { xA = xQ;}
    intyA=0;
    while (intyA <= fabs(sin(anginopenout * con)) * inlevOpendraw) {intyA= intyA+1;}
    if (anginopenout < 180) { yA = yQ - intyA;}
    if (anginopenout > 180) { yA = yQ + intyA;}
    if (anginopenout == 180) { yA = yQ;}
    pt[3] = NSMakePoint(xA,yA);
    
    //A2 Origin Coords}
    A2ang= anginA2out + anginA2j;
    intxA2=0;
    while (intxA2 <= fabs(cos(A2ang * con)) * A2jdraw) {intxA2= intxA2+1;}
    if (A2ang < 90) { xA2 = xQ + intxA2;}
    if (A2ang > 90) { xA2 = xQ - intxA2;}
    if (A2ang == 90) { xA2 = xQ;}
    intyA2=0;
    while (intyA2 >= fabs(sin(A2ang * con)) * A2jdraw) {intyA2= intyA2+1;}
    if (A2ang < 180) { yA2 = yQ + intyA2;}
    if (A2ang > 180) { yA2 = yQ - intyA2;}
    if (A2ang == 180) { yA2 = yQ;}
    pt[4] = NSMakePoint(xA2,yA2);
    
    //A3 insert coords}
    intxI=0;
    while (intxI <= fabs(cos(anginA3out * con)) * inlevA3draw) {intxI= intxI+1;}
    if (anginA3out < 90) { xI = xQ + intxI;}
    if (anginA3out > 90) { xI = xQ - intxI;}
    if (anginA3out == 90) { xI = xQ;}
    intyI=0;
    while (intyI <= fabs(sin(anginA3out * con)) * inlevA3draw) {intyI= intyI+1;}
    if (angA2A3<anginA2out) {yI = yQ + intyI;}
    if (angA2A3>anginA2out) {yI = yQ - intyI;}
    if (angA2A3==anginA2out) {yI = yQ;}
    pt[5] = NSMakePoint(xI,yI);
    
    //A3 Origin Coords}
    A3ang= anginA3out + anginA3j;
    intxA3=0;
    while (intxA3 <= fabs(cos(A3ang * con)) * A3jdraw) {intxA3= intxA3+1;}
    if (A3ang < 90) { xA3 = xQ + intxA3;}
    if (A3ang > 90) { xA3 = xQ - intxA3;}
    if (A3ang == 90) { xA3 = xQ;}
    intyA3=0;
    while (intyA3 <= fabs(sin(A3ang * con)) * A3jdraw) {intyA3= intyA3+1;}
    yA3 = yQ + intyA3;
    pt[6] = NSMakePoint(xA3,yA3);
    
    //Molar Bite Point Coords
    xMB = xD- round((xD-xC)* (1-molarpt));
    yMB = yC- round((yC-yD)* molarpt);
    pt[18] = NSMakePoint(xMB,yMB);
    
    //Skull Coords
    // pt[21] = NSMakePoint(xA2,yA);
    // pt[22] = NSMakePoint((xA3-20),(yA3-20));
    xskull1= xC-((xC-xA3)/2);
     pt[23] = NSMakePoint(xskull1,(yA3+40));
     pt[24] = NSMakePoint(xC,(yA3+60));
     pt[25] = NSMakePoint(xC+60,yA3+40);
     pt[26] = NSMakePoint(xC+100,yA3);
     pt[27] = NSMakePoint(xD,(yD+50));
     pt[28] = NSMakePoint(xD,(yD+20));
     pt[29] = NSMakePoint(xC,(yC+5));
    
     pt[30] = NSMakePoint(xC,yA3-20); //eye point
    
    //Tooth Coords
    // pt[27] = NSMakePoint((xD-xD/10),(yD+(xD/10))); //tooth tip
    // pt[28] = NSMakePoint((xD-xD/10),(yD)); //tooth base
    

    //*****************************************  Jaw Open
    //Quad joint and Jaw tip 
    xQ = xstart;
    yQ = ystart;
    if (jawrot <= 90) {
        intxD=0;
        while (intxD <= cos(jawrot * con) * outlevdraw) {intxD= intxD+1;}
        xD = xQ + intxD;}
    if (jawrot > 90) {
        intxD=0;
        while (intxD <= fabs(cos(jawrot * con)) * outlevdraw) {intxD= intxD+1;}
        xD = xQ - intxD;}
    intyD=0;
    while (intyD <= sin(jawrot * con) * outlevdraw) {intyD= intyD+1;}
    yD = yQ - intyD;
    pt[7] = NSMakePoint(xD,yD);
    
    //A2 insertion: Cor tip Coords
    corangn= anginA2out- jawrot;
    intxC=0;
    intyC=0;
    if (corangn >= 0) {
        while (intxC <= fabs(cos(corangn * con)) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ + intxC;
        while (intyC <= fabs(sin(corangn * con)) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ + intyC;}
    if (corangn < 0) {
        if (corangn > -90){
            while (intxC <= fabs(cos(corangn * con)) * inlevA2draw) {intxC= intxC+1;}
            xC= xQ + intxC;
            while (intyC <= fabs(sin(corangn * con)) * inlevA2draw) {intyC= intyC+1;}
            yC = yQ - intyC;}}
    if (corangn < -90) {
        while (intxC <= fabs(cos(corangn * con)) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ - intxC;
        while (intyC <= fabs(sin(corangn * con)) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ - intyC;}
    pt[8] = NSMakePoint(xC,yC);
    
    //Inlever: Articular Coords
    artangn= anginopenout + jawrot;
    intxA=0;
    intyA=0;
    if (artangn <= 90) { 
        while (intxA <= fabs(cos(artangn * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ + intxA;
        while (intyA <= fabs(sin(artangn * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ - intyA;}
    if (artangn > 90) { 
        while (intxA <= fabs(cos(artangn * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ - intxA;
        while (intyA <= fabs(sin(artangn * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ - intyA;}
    if (artangn > 180) { 
        while (intxA <= fabs(cos(artangn * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ - intxA;
        while (intyA <= fabs(sin(artangn * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ + intyA;}
    pt[9] = NSMakePoint(xA,yA);
    
    //A3 insert coords}
    A3angn= anginA3out-jawrot;
    intxI=0;
    intyI=0;
    if (A3angn >= 0) {
        while (intxI <= fabs(cos(A3angn * con)) * inlevA3draw) {intxI= intxI+1;}
        xI= xQ + intxI;
        while (intyI <= fabs(sin(A3angn * con)) * inlevA3draw) {intyI= intyI+1;}
        yI = yQ + intyI;}
    if (A3angn < 0) {
        if (A3angn > -90){
            while (intxI <= fabs(cos(A3angn * con)) * inlevA3draw) {intxI= intxI+1;}
            xI= xQ + intxI;
            while (intyI <= fabs(sin(A3angn * con)) * inlevA3draw) {intyI= intyI+1;}
            yI = yQ - intyI;}}
    if (A3angn < -90) {
        while (intxI <= fabs(cos(A3angn * con)) * inlevA3draw) {intxI= intxI+1;}
        xI= xQ - intxI;
        while (intyI <= fabs(sin(A3angn * con)) * inlevA3draw) {intyI= intyI+1;}
        yI = yQ - intyI;}
    pt[10] = NSMakePoint(xI,yI);
    
    //Molar Bite Point Coords
    xMB = xD- round((xD-xC)* (1-molarpt));
    yMB = yC- round((yC-yD)* molarpt);
    pt[19] = NSMakePoint(xMB,yMB);

//***************************************Jaw Muscles Contracted    
    //A2 Quad joint and Jaw tip
    xQ = xstart;
    yQ = ystart;
    if (jawrotA2c > 0) {
        if (jawrotA2c <= 90) {
            intxD=0;
            while (intxD <= fabs(cos(jawrotA2c * con)) * outlevdraw) {intxD= intxD+1;}
            xD = xQ + intxD;}
        if (jawrotA2c > 90) {
            intxD=0;
            while (intxD <= fabs(cos(jawrotA2c * con)) * outlevdraw) {intxD= intxD+1;}
            xD = xQ - intxD;}
        intyD=0;
        while (intyD <= fabs(sin(jawrotA2c * con)) * outlevdraw) {intyD= intyD+1;}
        yD = yQ - intyD;}
    
    if (jawrotA2c < 0) {
        tempang= fabs(jawrotA2c);
        intxD=0;
        while (intxD <= cos(tempang * con) * outlevdraw) {intxD= intxD+1;}
        xD = xQ + intxD;
        intyD=0;
        while (intyD <= sin(tempang * con) * outlevdraw) {intyD= intyD+1;}
        yD = yQ + intyD;}
    pt[11] = NSMakePoint(xD,yD);
    
    //A2 insertion: Cor tip Coords}
    corangc= anginA2out- jawrotA2c;
    intxC=0;
    intyC=0;
    if (corangc > 0) {
        while (intxC <= cos(corangc * con) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ + intxC;
        while (intyC <= sin(corangc * con) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ + intyC;}
    if (corangc > 90) {
        while (intxC <= cos(corangc * con) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ - intxC;
        while (intyC <= sin(corangc * con) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ + intyC;}
    if (corangc < 0) {
        tempang= fabs(corangc);
        while (intxC <= cos(tempang * con) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ + intxC;
        while (intyC <= sin(tempang * con) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ - intyC;}
    pt[12] = NSMakePoint(xC,yC);
    
    //Open Inlever: Articular Coords}
    artangc= anginopenout + jawrotA2c;
    intxA=0;
    intyA=0;
    if (artangc <= 90) { 
        while (intxA <= fabs(cos(artangc * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ + intxA;
        while (intyA <= fabs(sin(artangc * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ - intyA;}
    if (artangc > 90) { 
        while (intxA <= fabs(cos(artangc * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ - intxA;
        while (intyA <= fabs(sin(artangc * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ - intyA;}
    if (artangc > 180) { 
        while (intxA <= fabs(cos(artangc * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ - intxA;
        while (intyA <= fabs(sin(artangc * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ + intyA;}
    pt[13] = NSMakePoint(xA,yA);
    
    // A3: Quad joint and Jaw tip}
    xQ = xstart;
    yQ = ystart;
    if (jawrotA3c > 0) {
        intyD=0;
        while (intyD <= sin(jawrotA3c * con) * outlevdraw) {intyD= intyD+1;}
        yD = yQ - intyD;
        if (jawrotA3c <= 90) {
            intxD=0;
            while (intxD <= cos(jawrotA3c * con) * outlevdraw) {intxD= intxD+1;}
            xD = xQ + intxD;}
        if (jawrotA3c > 90) {
            intxD=0;
            while (intxD <= cos(jawrotA3c * con) * outlevdraw) {intxD= intxD+1;}
            xD = xQ - intxD;}}
    if (jawrotA3c < 0) {
        tempang= fabs(jawrotA3c);
        intyD=0;
        while (intyD <= sin(tempang * con) * outlevdraw) {intyD= intyD+1;}
        yD = yQ + intyD;
        if (jawrotA3c > -90) {
            intxD=0;
            while (intxD <= cos(tempang * con) * outlevdraw) {intxD= intxD+1;};
            xD = xQ + intxD;}
        if (jawrotA3c < -90) {
            intxD=0;
            while (intxD <= cos(tempang * con) * outlevdraw) {intxD= intxD+1;}
            xD = xQ - intxD;}}
    pt[14] = NSMakePoint(xD,yD);
    
    //A2 insertion: Cor tip Coords based on A3 cont}
    corangc= anginA2out- jawrotA3c;
    intxC=0;
    intyC=0;
    if (corangc > 0) {
        while (intxC <= fabs(cos(corangc * con)) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ + intxC;
        while (intyC <= fabs(sin(corangc * con)) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ + intyC;}
    if (corangc > 90) {
        while (intxC <= fabs(cos(corangc * con)) * inlevA2draw){intxC= intxC+1;}
        xC= xQ - intxC;
        while (intyC <= fabs(sin(corangc * con)) * inlevA2draw) {intyC= intyC+1;}
        yC = yQ + intyC;}
    if (corangc < 0) {
        tempang= fabs(corangc);
        while (intxC <= cos(tempang * con) * inlevA2draw) {intxC= intxC+1;}
        xC= xQ + intxC;
        while (intyC <= sin(tempang * con) * inlevA2draw) {intyC= intyC+1;};
        yC = yQ - intyC;}
    pt[15] = NSMakePoint(xC,yC);
    
    //Open Inlever: Articular Coords based on A3 cont
    artangc= anginopenout + jawrotA3c;
    intxA=0;
    intyA=0;
    if (artangc <= 90) { 
        while (intxA <= fabs(cos(artangc * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ + intxA;
        while (intyA <= fabs(sin(artangc * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ - intyA;}
    if (artangc > 90) { 
        while (intxA <= fabs(cos(artangc * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ - intxA;
        while (intyA <= fabs(sin(artangc * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ - intyA;}
    if (artangc > 180) { 
        while (intxA <= fabs(cos(artangc * con)) * inlevOpendraw) {intxA= intxA+1;}
        xA = xQ - intxA;
        while (intyA <= fabs(sin(artangc * con)) * inlevOpendraw) {intyA= intyA+1;}
        yA = yQ + intyA;}
    pt[16] = NSMakePoint(xA,yA);
    
    //A3 insert coords}
    A3angc= anginA3out-jawrotA3c;
    intxI=0;
    intyI=0;
    if (A3angc >= 0) {
        while (intxI <= fabs(cos(A3angc * con)) * inlevA3draw) {intxI= intxI+1;}
        xI= xQ + intxI;
        while (intyI <= fabs(sin(A3angc * con)) * inlevA3draw) {intyI= intyI+1;}
        yI = yQ + intyI;}
    if (A3angc < 0) {
        if (A3angc > -90){
            while (intxI <= fabs(cos(A3angc * con)) * inlevA3draw) {intxI= intxI+1;}
            xI= xQ + intxI;
            while (intyI <= fabs(sin(A3angc * con)) * inlevA3draw) {intyI= intyI+1;}
            yI = yQ - intyI;}}
    if (A3angc < -90) {
        while (intxI <= fabs(cos(A3angc * con)) * inlevA3draw) {intxI= intxI+1;}
        xI= xQ - intxI;
        while (intyI <= fabs(sin(A3angc * con)) * inlevA3draw) {intyI= intyI+1;}
        yI = yQ - intyI;}
    pt[17] = NSMakePoint(xI,yI);
    
    //Molar Bite Point Coords
    xMB = xD- round((xD-xC)* (1-molarpt));
    yMB = yC- round((yC-yD)* molarpt);
    pt[20] = NSMakePoint(xMB,yMB);
}

//---------------------------------------------------------------------------------
- (IBAction)musclesim:(id)sender
{
    percentA2= (A2n-A2)/A2n;
    unitpercentA2= percentA2/bins;
    bindistA2= (A2n-A2)/bins;
    percentA3= (A3actingn-A3acting)/A3actingn;
    unitpercentA3= percentA3/bins;
    bindistA3= (A3actingn-A3acting)/bins;
    VVmax= peakVVhill;
    FFmaxA2= minFFhill;
    FFmaxA3= minFFhill;
    unitVmax= (peakVVhill-minVVhill)/bins;
    z=0;
    while (z<bins){
        simpercentA2=simpercentA2+unitpercentA2;
        simpercentA3=simpercentA3+unitpercentA3;
        VVmax= VVmax - unitVmax;
        FFmaxA2= (k-(k*VVmax))/(k+VVmax);
        FFmaxA3= (k-(k*VVmax))/(k+VVmax);
        [self processdata2:nil];
        [self scaledraw:nil];
        [self drawcoords:nil];
        z=z+1;}
}

//---------------------------------------------------------------------------------
//Data To Screen
- (IBAction)datatoscreen:(id)sender {
    [data7 setStringValue: [specarray objectAtIndex: i]];
    [data1 setStringValue:[NSString stringWithFormat:@"%f",inlevA2]];
    [data2 setStringValue:[NSString stringWithFormat:@"%f",inlevA3]];
    [data3 setStringValue:[NSString stringWithFormat:@"%f",inlevOpen]];
    [data4 setStringValue:[NSString stringWithFormat:@"%f",outlever]];
    [data5 setStringValue:[NSString stringWithFormat:@"%f",A2]];
    [data6 setStringValue:[NSString stringWithFormat:@"%f",A3]];
    [data8 setStringValue:[NSString stringWithFormat:@"%f",A3t]];
    [data9 setStringValue:[NSString stringWithFormat:@"%f",A2j]];
    [data10 setStringValue:[NSString stringWithFormat:@"%f",A3j]];
    [data11 setStringValue:[NSString stringWithFormat:@"%f",A2A3ins]];
    [data12 setStringValue:[NSString stringWithFormat:@"%f",ljtop]];
    [data13 setStringValue:[NSString stringWithFormat:@"%f",ljbot]];
    [data14 setStringValue:[NSString stringWithFormat:@"%f",massA2]];
    [data15 setStringValue:[NSString stringWithFormat:@"%f",massA3]];
    
    [data17 setStringValue:[NSString stringWithFormat:@"%f",MAA2]];
    [data18 setStringValue:[NSString stringWithFormat:@"%f",emaA2]];
    [data19 setStringValue:[NSString stringWithFormat:@"%f",anginA2]];
    [data20 setStringValue:[NSString stringWithFormat:@"%f",anginA2c]];
    [data21 setStringValue:[NSString stringWithFormat:@"%f",A2durtot]];
    [data22 setStringValue:[NSString stringWithFormat:@"%f",A2n]];
    [data23 setStringValue:[NSString stringWithFormat:@"%f",gapecontA2]];
    
    [data24 setStringValue:[NSString stringWithFormat:@"%f",MAA3]];
    [data25 setStringValue:[NSString stringWithFormat:@"%f",emaA3]];
    [data26 setStringValue:[NSString stringWithFormat:@"%f",anginA3]];
    [data27 setStringValue:[NSString stringWithFormat:@"%f",anginA3c]];
    [data28 setStringValue:[NSString stringWithFormat:@"%f",A3durtot]];
    [data29 setStringValue:[NSString stringWithFormat:@"%f",A3n]];
    [data30 setStringValue:[NSString stringWithFormat:@"%f",gapecontA3]];
    
    [data31 setStringValue:[NSString stringWithFormat:@"%f",LJrotA2c]];
    [data32 setStringValue:[NSString stringWithFormat:@"%f",LJrotA3c]];
    [data33 setStringValue:[NSString stringWithFormat:@"%f",angvelA2]];
    [data34 setStringValue:[NSString stringWithFormat:@"%f",angvelA3]];
    [data35 setStringValue:[NSString stringWithFormat:@"%f",gapevelA2]];
    [data36 setStringValue:[NSString stringWithFormat:@"%f",gapevelA3]];
    [data37 setStringValue:[NSString stringWithFormat:@"%f",gapeOpen]];
    [data38 setStringValue:[NSString stringWithFormat:@"%f",gapeOpen]];
    [data39 setStringValue:[NSString stringWithFormat:@"%i",i]];
    
    [sim1 setStringValue:[NSString stringWithFormat:@"%.1f",jawrot]];
    [sim2 setStringValue:[NSString stringWithFormat:@"%.2f",peakVVhill]];
    [sim3 setStringValue:[NSString stringWithFormat:@"%.2f",minVVhill]];
    [sim4 setStringValue:[NSString stringWithFormat:@"%.2f",peakFFhill]];
    [sim5 setStringValue:[NSString stringWithFormat:@"%.2f",minFFhill]];
    [sim6 setStringValue:[NSString stringWithFormat:@"%.1f",mfArea]];
    [sim7 setStringValue:[NSString stringWithFormat:@"%.1f",vmax]];
    [sim8 setStringValue:[NSString stringWithFormat:@"%i",bins]];
    [sim9 setStringValue:[NSString stringWithFormat:@"%i",opendur]];
    [sim10 setStringValue:[NSString stringWithFormat:@"%.1f",pinA2]];
    [sim11 setStringValue:[NSString stringWithFormat:@"%.1f",pinA3]];
    
    [bite1 setStringValue:[NSString stringWithFormat:@"%f",biteforcehalf]];
    [bite2 setStringValue:[NSString stringWithFormat:@"%f",biteforcebilat]];
    [bite3 setStringValue:[NSString stringWithFormat:@"%f",maxbiteforce]];
    [bite4 setStringValue:[NSString stringWithFormat:@"%f",biteforcehalfm]];
    [bite5 setStringValue:[NSString stringWithFormat:@"%f",biteforcebilatm]];
    [bite6 setStringValue:[NSString stringWithFormat:@"%f",maxbiteforcem]];
    [bite7 setStringValue:[NSString stringWithFormat:@"%f",molarpt]];

    [mus1 setStringValue:[NSString stringWithFormat:@"%f",mAreaA2]];
    [mus2 setStringValue:[NSString stringWithFormat:@"%f",contdistA2]];
    [mus3 setStringValue:[NSString stringWithFormat:@"%f",mforceA2]];
    [mus4 setStringValue:[NSString stringWithFormat:@"%f",mforceactA2]];
    [mus5 setStringValue:[NSString stringWithFormat:@"%f",torqueA2]];
    [mus6 setStringValue:[NSString stringWithFormat:@"%f",workA2mj]];
    [mus7 setStringValue:[NSString stringWithFormat:@"%f",powerA2]];
    [mus8 setStringValue:[NSString stringWithFormat:@"%f",powerA2W]];
    [mus9 setStringValue:[NSString stringWithFormat:@"%f",mAreaA3]];
    [mus10 setStringValue:[NSString stringWithFormat:@"%f",contdistA3]];
    [mus11 setStringValue:[NSString stringWithFormat:@"%f",mforceA3]];
    [mus12 setStringValue:[NSString stringWithFormat:@"%f",mforceactA3]];
    [mus13 setStringValue:[NSString stringWithFormat:@"%f",torqueA3]];
    [mus14 setStringValue:[NSString stringWithFormat:@"%f",workA3mj]];
    [mus15 setStringValue:[NSString stringWithFormat:@"%f",powerA3]];
    [mus16 setStringValue:[NSString stringWithFormat:@"%f",powerA3W]];
    [mus17 setStringValue:[NSString stringWithFormat:@"%f",forceoutA2]];
    [mus18 setStringValue:[NSString stringWithFormat:@"%f",forceoutA3]];
}
//---------------------------------------------------------------------------------
- (IBAction)nextJaw:(id)sender {
    if (i<fishcount){
        [self checkvars:nil];
        i=i+1; 
        [self zerovars:nil];
        [self errorcheck:nil];
        [self processdata1:nil];
        [self musclesim:nil];
        [self datatoscreen:nil];
        [self setNeedsDisplay:YES];}
}
//---------------------------------------------------------------------------------
- (IBAction)prevJaw:(id)sender {
    if (i>1){
        [self checkvars:nil];
        i=i-1; 
        [self zerovars:nil];
        [self errorcheck:nil];
        [self processdata1:nil];
        [self musclesim:nil];
        [self datatoscreen:nil];
        [self setNeedsDisplay:YES];}
}
//---------------------------------------------------------------------------------
- (IBAction)contmore:(id)sender { //close more button
   if (newbins<bins){
        [self checkvars:nil];
        newbins = newbins+1;
        [self zerovars:nil];
        [self processdata1:nil];
        percentA2= (A2n-A2)/A2n;
        unitpercentA2= percentA2/bins;
        bindistA2= (A2n-A2)/bins;
        percentA3= (A3actingn-A3acting)/A3actingn;
        unitpercentA3= percentA3/bins;
        bindistA3= (A3actingn-A3acting)/bins;
        VVmax= peakVVhill;
        FFmaxA2= minFFhill;
        FFmaxA3= minFFhill;
        unitVmax= (peakVVhill-minVVhill)/bins;
        z=0;
        while (z<newbins){
            simpercentA2=simpercentA2+unitpercentA2;
            simpercentA3=simpercentA3+unitpercentA3;
            VVmax= VVmax - unitVmax;
            FFmaxA2= (k-(k*VVmax))/(k+VVmax);
            FFmaxA3= (k-(k*VVmax))/(k+VVmax);
            [self processdata2:nil];
            [self scaledraw:nil];
            [self drawcoords:nil];
            z=z+1;}
    
       [self setNeedsDisplay:YES];
       }
}

//---------------------------------------------------------------------------------
- (IBAction)contless:(id)sender //open more button
{
    if (newbins>1){
        [self checkvars:nil];
        newbins = newbins-1;
        [self zerovars:nil];
        [self processdata1:nil];
        percentA2= (A2n-A2)/A2n;
        unitpercentA2= percentA2/bins;
        bindistA2= (A2n-A2)/bins;
        percentA3= (A3actingn-A3acting)/A3actingn;
        unitpercentA3= percentA3/bins;
        bindistA3= (A3actingn-A3acting)/bins;
        VVmax= peakVVhill;
        FFmaxA2= minFFhill;
        FFmaxA3= minFFhill;
        unitVmax= (peakVVhill-minVVhill)/bins;
        z=0;
        while (z<newbins){
            simpercentA2=simpercentA2+unitpercentA2;
            simpercentA3=simpercentA3+unitpercentA3;
            VVmax= VVmax - unitVmax;
            FFmaxA2= (k-(k*VVmax))/(k+VVmax);
            FFmaxA3= (k-(k*VVmax))/(k+VVmax);
            [self processdata2:nil];
            [self scaledraw:nil];
            [self drawcoords:nil];
            z=z+1;}
     
        [self setNeedsDisplay:YES];
    }
}
//---------------------------------------------------------------------------------
- (IBAction)magnify:(id)sender
{
    scale= scale+20;
    [self checkvars:nil];
    //[self zerovars:nil];
    //[self processdata1:nil];
    //[self processdata2:nil];
    //[self musclesim:nil];
    [self scaledraw:nil];
    [self drawcoords:nil];
    [self setNeedsDisplay:YES];
    graphiccheck=1;
}
//---------------------------------------------------------------------------------
- (IBAction)reduce:(id)sender
{
    scale= scale-20;
    [self checkvars:nil];
    //[self zerovars:nil];
    //[self processdata1:nil];
    //[self processdata2:nil];
    //[self musclesim:nil];
    [self scaledraw:nil];
    [self drawcoords:nil];
    [self setNeedsDisplay:YES];
    graphiccheck=1;
}
//---------------------------------------------------------------------------------
- (IBAction)upmove:(id)sender
{
    ystart= ystart+20;
    //[self zerovars:nil];
    //[self processdata1:nil];
    //[self musclesim:nil];
    [self scaledraw:nil];
    [self drawcoords:nil];
    [self setNeedsDisplay:YES];
    graphiccheck=1;
}
//---------------------------------------------------------------------------------
- (IBAction)downmove:(id)sender
{
    ystart= ystart-20;
    //[self zerovars:nil];
    //[self processdata1:nil];
    //[self musclesim:nil];
    [self scaledraw:nil];
    [self drawcoords:nil];
    [self setNeedsDisplay:YES];
    graphiccheck=1;
}
//---------------------------------------------------------------------------------
- (IBAction)leftmove:(id)sender
{
    xstart= xstart-20;
    //[self zerovars:nil];
    //[self processdata1:nil];
    //[self musclesim:nil];
    [self scaledraw:nil];
    [self drawcoords:nil];
    [self setNeedsDisplay:YES];
    graphiccheck=1;
}
//---------------------------------------------------------------------------------
- (IBAction)rightmove:(id)sender
{
    xstart= xstart+20;
    //[self zerovars:nil];
    //[self processdata1:nil];
    //[self musclesim:nil];
    [self scaledraw:nil];
    [self drawcoords:nil];
    [self setNeedsDisplay:YES];
    graphiccheck=1;
}

//---------------------------------------------------------------------------------
// Open File, read in data, and calc everything
- (IBAction)doOpen:(id)sender {
    NSOpenPanel *theOpenPanel = [NSOpenPanel openPanel];
    [theOpenPanel setTitle:@"Choose a File to Open"];
    if ([theOpenPanel runModal] == NSOKButton) 
    {
        theFilePath = [theOpenPanel filename];
        NSString *s = [NSString stringWithContentsOfFile:theFilePath encoding:NSUTF8StringEncoding error:nil];
        i=0;
        NSScanner *scanner = [NSScanner scannerWithString:s];
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        i=0;
        while (i<fishcount) {
            i=i+1; 
            [self zeroarrays:nil];
        }
        i=0;
        while ([scanner isAtEnd] == NO) {
            fishok = 0;
            i= i+1;
            [scanner scanUpToCharactersFromSet:whiteSpace intoString:&spec];
            [specarray insertObject:spec atIndex:i];
            [scanner scanDouble:&inlevA2];
            [inlevA2array insertObject:[NSNumber numberWithDouble:inlevA2] atIndex:i];
            [scanner scanDouble:&inlevA3];
            [inlevA3array insertObject:[NSNumber numberWithDouble:inlevA3] atIndex:i];
            [scanner scanDouble:&inlevOpen];
            [inlevOpenarray insertObject:[NSNumber numberWithDouble:inlevOpen] atIndex:i];
            [scanner scanDouble:&outlever];
            [outleverarray insertObject:[NSNumber numberWithDouble:outlever] atIndex:i];
            [scanner scanDouble:&A2];
            [A2array insertObject:[NSNumber numberWithDouble:A2] atIndex:i];
            [scanner scanDouble:&A3];
            [A3array insertObject:[NSNumber numberWithDouble:A3] atIndex:i];
            [scanner scanDouble:&A3t];
            [A3tarray insertObject:[NSNumber numberWithDouble:A3t] atIndex:i];
            [scanner scanDouble:&A2j];
            [A2jarray insertObject:[NSNumber numberWithDouble:A2j] atIndex:i];
            [scanner scanDouble:&A3j];
            [A3jarray insertObject:[NSNumber numberWithDouble:A3j] atIndex:i];
            [scanner scanDouble:&A2A3ins];
            [A2A3insarray insertObject:[NSNumber numberWithDouble:A2A3ins] atIndex:i];
            [scanner scanDouble:&ljtop];
            [ljtoparray insertObject:[NSNumber numberWithDouble:ljtop] atIndex:i];
            [scanner scanDouble:&ljbot];
            [ljbotarray insertObject:[NSNumber numberWithDouble:ljbot] atIndex:i];
            [scanner scanDouble:&massA2];
            [massA2array insertObject:[NSNumber numberWithDouble:massA2] atIndex:i];
            [scanner scanDouble:&massA3];
            [massA3array insertObject:[NSNumber numberWithDouble:massA3] atIndex:i];

            if (massA3>0) {fishok = 1;}
            if (fishok==0) {
                errspec = [NSString stringWithFormat:@"%d", i];
                NSAlert *alert = [[NSAlert alloc] init];
                [alert setAlertStyle:NSInformationalAlertStyle];
                [alert addButtonWithTitle:@"OK"];
                [alert setMessageText:@"Error: Reading file, number of data items in a line, in specimen #"];
                [alert setInformativeText: errspec];
                [alert runModal];
                [alert release];}
            fishcount=i;
            [self errorcheck:nil];
        }
        
        if (fishcount>0) {fileopen=1;}
        i=1; 
        [self initvars:nil];
        [self zerovars:nil];
        [self processdata1:nil];
        [self musclesim:nil];
        //[self drawcoords:nil];
        //[self scaledraw:nil];
        [self datatoscreen:nil];
        [self setNeedsDisplay:YES]; 
    }
}
//---------------------------------------------------------------------------------
// Open File, read in data, and calc everything
- (IBAction)doCoordOpen:(id)sender {
    NSOpenPanel *theOpenPanel = [NSOpenPanel openPanel];
    [theOpenPanel setTitle:@"Choose a Coordinate File to Open"];
    if ([theOpenPanel runModal] == NSOKButton) 
    {
        theFilePath = [theOpenPanel filename];
        NSString *s = [NSString stringWithContentsOfFile:theFilePath encoding:NSUTF8StringEncoding error:nil];
        i=0;
        NSScanner *scanner = [NSScanner scannerWithString:s];
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        //specarray = [[NSMutableArray alloc] init]; 
        //[specarray insertObject:@"zero" atIndex:0]; 
        while ([scanner isAtEnd] == NO) {
            fishok = 0;
            i= i+1;
            [scanner scanUpToCharactersFromSet:whiteSpace intoString:&spec];
            [specarray insertObject:spec atIndex:i];
            [scanner scanDouble:&x1];
            [scanner scanDouble:&y1];
            [scanner scanDouble:&x2];
            [scanner scanDouble:&y2];
            [scanner scanDouble:&x3];
            [scanner scanDouble:&y3];
            [scanner scanDouble:&x4];
            [scanner scanDouble:&y4];
            [scanner scanDouble:&x5];
            [scanner scanDouble:&y5];
            [scanner scanDouble:&x6];
            [scanner scanDouble:&y6];
            [scanner scanDouble:&x7];
            [scanner scanDouble:&y7];
            [scanner scanDouble:&x8];
            [scanner scanDouble:&y8];
            [scanner scanDouble:&massA2];
            [scanner scanDouble:&massA3];
            inlevA2= sqrt((x5-x1)*(x5-x1)+(y5-y1)*(y5-y1));
            inlevA3= sqrt((x7-x1)*(x7-x1)+(y7-y1)*(y7-y1));
            inlevOpen= sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1));
            outlever= sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
            A2= sqrt((x4-x5)*(x4-x5)+(y4-y5)*(y4-y5));
            A3= sqrt((x6-x7)*(x6-x7)+(y6-y7)*(y6-y7));
            A3t= sqrt((x8-x7)*(x8-x7)+(y8-y7)*(y8-y7));
            A2j= sqrt((x4-x1)*(x4-x1)+(y4-y1)*(y4-y1));
            A3j= sqrt((x6-x1)*(x6-x1)+(y6-y1)*(y6-y1));
            A2A3ins= sqrt((x7-x5)*(x7-x5)+(y7-y5)*(y7-y5));
            ljtop= sqrt((x5-x2)*(x5-x2)+(y5-y2)*(y5-y2));
            ljbot= sqrt((x3-x2)*(x3-x2)+(y3-y2)*(y3-y2));
            [inlevA2array insertObject:[NSNumber numberWithDouble:inlevA2] atIndex:i];
            [inlevA3array insertObject:[NSNumber numberWithDouble:inlevA3] atIndex:i];
            [inlevOpenarray insertObject:[NSNumber numberWithDouble:inlevOpen] atIndex:i];
            [outleverarray insertObject:[NSNumber numberWithDouble:outlever] atIndex:i];
            [A2array insertObject:[NSNumber numberWithDouble:A2] atIndex:i];
            [A3array insertObject:[NSNumber numberWithDouble:A3] atIndex:i];
            [A3tarray insertObject:[NSNumber numberWithDouble:A3t] atIndex:i];
            [A2jarray insertObject:[NSNumber numberWithDouble:A2j] atIndex:i];
            [A3jarray insertObject:[NSNumber numberWithDouble:A3j] atIndex:i];
            [A2A3insarray insertObject:[NSNumber numberWithDouble:A2A3ins] atIndex:i];
            [ljtoparray insertObject:[NSNumber numberWithDouble:ljtop] atIndex:i];
            [ljbotarray insertObject:[NSNumber numberWithDouble:ljbot] atIndex:i];
            [massA2array insertObject:[NSNumber numberWithDouble:massA2] atIndex:i];
            [massA3array insertObject:[NSNumber numberWithDouble:massA3] atIndex:i];

            if (massA3>0) {fishok = 1;}
            if (fishok==0) {
                errspec = [NSString stringWithFormat:@"%d", i];
                NSAlert *alert = [[NSAlert alloc] init];
                [alert setAlertStyle:NSInformationalAlertStyle];
                [alert addButtonWithTitle:@"OK"];
                [alert setMessageText:@"Error: Reading file, number of data items in a line, in specimen #"];
                [alert setInformativeText: errspec];
                [alert runModal];
                [alert release];}
            fishcount=i;
        }
        if (fishcount>0) {fileopen=1;}
        i=1; 
        [self errorcheck:nil];
        [self initvars:nil];
        [self zerovars:nil];
        [self processdata1:nil];
        [self musclesim:nil];
        [self datatoscreen:nil];
        [self setNeedsDisplay:YES]; 
    }
}

//---------------------------------------------------------------------------------
// Save datafile.
- (IBAction)savedata:(id)sender
{
    NSMutableData *header1, *header2, *header3, *header4;  //Set up file headers to give labels to columns
    const char *head1 = " A2Data Spec LJangle(deg) Gape(cm) BiteFA2(N) TotBilatBiteF(N) VVmaxA2 FFmaxA2 TimeA2(ms) A2c(cm) A2cont% A2Fact(N) Torque(Nm) EMA AngVel(deg/ms) GapeVel(cm/ms) A2Work(mJ) A2Power(W) A2Power(W/kg)"; //A2 dynamic output- all bins
    const char *head2 = " A3Data Spec LJangle(deg) Gape(cm) BiteFA3(N) TotBilatBiteF(N) VVmaxA3 FFmaxA3 TimeA3(ms) A3c(cm) A3cont% A3Fact(N) Torque(Nm) EMA AngVel(deg/ms) GapeVel(cm/ms) A3Work(mJ) A3Power(W) A3Power(W/kg)";//A3 dynamic output- all bins
    const char *head3 = " OpenSum Spec JawAng(deg) Gape(cm) OpenDur(ms) MAOpen VROpen AngV(deg/ms) VOpen(cm/ms)";// Opening summary for all fish
    const char *head4 = " CloseSum Spec JawAng(deg) BiteFA2(N) BiteFA3(N) TotBiteF(N) MaxBite(N) MAA2 MAA3 MusF(kPa) A2csa(cm2) FmaxA2(N) xFA2(N) xTqA2(Nm) WkA2(mJ) PwA2(W) A3csa(cm2) FmaxA3(N) xFA3(N) xTqA3(Nm) WkA3(mJ) PwA3(W) Vmax(l/s) HiVVmax(%) LoVVmax(%) HiFFmax(%) LoFFmax(%)";// Closing summary for all fish
    header1 = [NSMutableData dataWithBytes:head1 length:strlen(head1)];
    header2 = [NSMutableData dataWithBytes:head2 length:strlen(head2)];
    header3 = [NSMutableData dataWithBytes:head3 length:strlen(head3)];
    header4 = [NSMutableData dataWithBytes:head4 length:strlen(head4)];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create array, store documents directory
    NSString *documentsDirectory = [paths objectAtIndex:0]; //hold path to documents directory
    NSString *outFilePath1 = [documentsDirectory stringByAppendingPathComponent:@"*A2sim.txt"];
    [header1 writeToFile:outFilePath1 atomically:YES];
    NSString *outFilePath2 = [documentsDirectory stringByAppendingPathComponent:@"*A3sim.txt"];
    [header2 writeToFile:outFilePath2 atomically:YES];
    NSString *outFilePath3 = [documentsDirectory stringByAppendingPathComponent:@"*OpenSum.txt"];
    [header3 writeToFile:outFilePath3 atomically:YES];
    NSString *outFilePath4 = [documentsDirectory stringByAppendingPathComponent:@"*CloseSum.txt"];
    [header4 writeToFile:outFilePath4 atomically:YES];
    
    NSFileHandle *fileHandle1 = [NSFileHandle fileHandleForWritingAtPath:outFilePath1];
    [fileHandle1 seekToEndOfFile];
    NSFileHandle *fileHandle2 = [NSFileHandle fileHandleForWritingAtPath:outFilePath2];
    [fileHandle2 seekToEndOfFile];
    NSFileHandle *fileHandle3 = [NSFileHandle fileHandleForWritingAtPath:outFilePath3];
    [fileHandle3 seekToEndOfFile];
    NSFileHandle *fileHandle4 = [NSFileHandle fileHandleForWritingAtPath:outFilePath4];
    [fileHandle4 seekToEndOfFile];
    
    i=0;
    while (i<fishcount) {
        i=i+1; 
        NSString *specname =[specarray objectAtIndex: i];
        //[self initvars:nil];
        [self checkvars:nil];
        [self zerovars:nil];
        [self processdata1:nil];
        percentA2= (A2n-A2)/A2n;
        unitpercentA2= percentA2/bins;
        bindistA2= (A2n-A2)/bins;
        percentA3= (A3actingn-A3acting)/A3actingn;
        unitpercentA3= percentA3/bins;
        bindistA3= (A3actingn-A3acting)/bins;
        VVmax= peakVVhill;
        FFmaxA2= minFFhill;
        FFmaxA3= minFFhill;
        unitVmax= (peakVVhill-minVVhill)/bins;
        z=0;
        while (z<bins){
            simpercentA2=simpercentA2+unitpercentA2;
            simpercentA3=simpercentA3+unitpercentA3;
            VVmax= VVmax - unitVmax;
            FFmaxA2= (k-(k*VVmax))/(k+VVmax);
            FFmaxA3= (k-(k*VVmax))/(k+VVmax);
            [self processdata2:nil];
            NSString *A2Data = [NSString stringWithFormat:@"\r\n %d %@ %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.6f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f", i, specname, jawrotA2c, gapecontA2, forceoutA2, biteforcebilat, VVmax, FFmaxA2, A2durtot, A2c, percentA2num, mforceactA2, torqueA2, emaA2, angvelA2, gapevelA2, workA2mj, powerA2, powerA2W];
            NSString *A3Data = [NSString stringWithFormat:@"\r\n %d %@ %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.6f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f", i, specname, jawrotA3c, gapecontA3, forceoutA3, biteforcebilat, VVmax, FFmaxA3, A3durtot, A3c, percentA3num, mforceactA3, torqueA3, emaA3, angvelA3, gapevelA3, workA3mj, powerA3, powerA3W];
            
            [fileHandle1 writeData:[A2Data dataUsingEncoding:NSUTF8StringEncoding]];
            [fileHandle2 writeData:[A3Data dataUsingEncoding:NSUTF8StringEncoding]];
            z=z+1;}
        
        NSString *openSumData = [NSString stringWithFormat:@"\r\n %d %@ %1.4f %1.4f %d %1.4f %1.4f %1.4f %1.4f", i, specname, jawrot, gapeOpen, opendur, MAOpen, VROpen, angVelopen, velOpen];
        [fileHandle3 writeData:[openSumData dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *closeSumData = [NSString stringWithFormat:@"\r\n %d %@ %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f %1.4f", i, specname, jawrot, forceoutA2, forceoutA3, biteforcebilat, maxbiteforce, MAA2, MAA3, mfArea, mAreaA2, mforceA2, meanforceA2, meantorqueA2, totworkA2mj, totpowerA2W, mAreaA3, mforceA3, meanforceA3, meantorqueA3, totworkA3mj, totpowerA3W, vmax, peakVVhill, minVVhill, peakFFhill, minFFhill];
        [fileHandle4 writeData:[closeSumData dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    [fileHandle1 closeFile];
    [fileHandle2 closeFile];
    [fileHandle3 closeFile];
    [fileHandle4 closeFile];
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:@"File Save"];
    [alert setInformativeText:@"The Output Files Were Saved in Documents Folder"];
    [alert runModal];
    [alert release];
}

//---------------------------------------------------------------------------------

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //sleep(2);
}

//---------------------------------------------------------------------------------
- (IBAction)changeSegment:(id)sender
{
	selectedSegment = [sender selectedSegment];
    [self setNeedsDisplay:YES];
}

//---------------------------------------------------------------------------------
- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        //Load up a sample data file
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SampleData" ofType:@"txt"];  
        NSString *s = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        i=0;
        NSScanner *scanner = [NSScanner scannerWithString:s];
        NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        specarray = [[NSMutableArray alloc] init];
        [specarray insertObject:@"zero" atIndex:0]; 
        inlevA2array = [[NSMutableArray alloc] init];
        [inlevA2array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        inlevA3array = [[NSMutableArray alloc] init];
        [inlevA3array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        inlevOpenarray = [[NSMutableArray alloc] init];
        [inlevOpenarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        outleverarray = [[NSMutableArray alloc] init];
        [outleverarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        A2array = [[NSMutableArray alloc] init];
        [A2array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        A3array = [[NSMutableArray alloc] init];
        [A3array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        A3tarray = [[NSMutableArray alloc] init];
        [A3tarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        A2jarray = [[NSMutableArray alloc] init];
        [A2jarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        A3jarray = [[NSMutableArray alloc] init];
        [A3jarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        A2A3insarray = [[NSMutableArray alloc] init];
        [A2A3insarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        ljtoparray = [[NSMutableArray alloc] init];
        [ljtoparray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        ljbotarray = [[NSMutableArray alloc] init];
        [ljbotarray insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        massA2array = [[NSMutableArray alloc] init];
        [massA2array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
        massA3array = [[NSMutableArray alloc] init];
        [massA3array insertObject:[NSNumber numberWithDouble:0] atIndex:i];
               while ([scanner isAtEnd] == NO) 
        {
            i= i+1;
            [scanner scanUpToCharactersFromSet:whiteSpace intoString:&spec];
            [specarray insertObject:spec atIndex:i];
            [scanner scanDouble:&inlevA2];
            [inlevA2array insertObject:[NSNumber numberWithDouble:inlevA2] atIndex:i];
            [scanner scanDouble:&inlevA3];
            [inlevA3array insertObject:[NSNumber numberWithDouble:inlevA3] atIndex:i];
            [scanner scanDouble:&inlevOpen];
            [inlevOpenarray insertObject:[NSNumber numberWithDouble:inlevOpen] atIndex:i];
            [scanner scanDouble:&outlever];
            [outleverarray insertObject:[NSNumber numberWithDouble:outlever] atIndex:i];
            [scanner scanDouble:&A2];
            [A2array insertObject:[NSNumber numberWithDouble:A2] atIndex:i];
            [scanner scanDouble:&A3];
            [A3array insertObject:[NSNumber numberWithDouble:A3] atIndex:i];
            [scanner scanDouble:&A3t];
            [A3tarray insertObject:[NSNumber numberWithDouble:A3t] atIndex:i];
            [scanner scanDouble:&A2j];
            [A2jarray insertObject:[NSNumber numberWithDouble:A2j] atIndex:i];
            [scanner scanDouble:&A3j];
            [A3jarray insertObject:[NSNumber numberWithDouble:A3j] atIndex:i];
            [scanner scanDouble:&A2A3ins];
            [A2A3insarray insertObject:[NSNumber numberWithDouble:A2A3ins] atIndex:i];
            [scanner scanDouble:&ljtop];
            [ljtoparray insertObject:[NSNumber numberWithDouble:ljtop] atIndex:i];
            [scanner scanDouble:&ljbot];
            [ljbotarray insertObject:[NSNumber numberWithDouble:ljbot] atIndex:i];
            [scanner scanDouble:&massA2];
            [massA2array insertObject:[NSNumber numberWithDouble:massA2] atIndex:i];
            [scanner scanDouble:&massA3];
            [massA3array insertObject:[NSNumber numberWithDouble:massA3] atIndex:i];
            fishcount =i;
        }
        i=1; 
        [self initvars:nil];
        [self zerovars:nil];
        [self processdata1:nil];
        [self musclesim:nil];
        [self datatoscreen:nil];
    }
    return self;
}

//---------------------------------------------------------------------------------
- (IBAction)drawSkull:(id)sender
{
    CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
    //Draw Skull
    //CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);                
    //CGContextAddLineToPoint(context,NSPointToCGPoint(pt[21]).x, NSPointToCGPoint (pt[21]).y);
    //CGContextAddLineToPoint(context,NSPointToCGPoint(pt[22]).x, NSPointToCGPoint (pt[22]).y);
    CGContextMoveToPoint(context,NSPointToCGPoint(pt[23]).x, NSPointToCGPoint (pt[23]).y);
    //CGContextAddLineToPoint(context,NSPointToCGPoint(pt[23]).x, NSPointToCGPoint (pt[23]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[24]).x, NSPointToCGPoint (pt[24]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[25]).x, NSPointToCGPoint (pt[25]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[26]).x, NSPointToCGPoint (pt[26]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[27]).x, NSPointToCGPoint (pt[27]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[28]).x, NSPointToCGPoint (pt[28]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[29]).x, NSPointToCGPoint (pt[29]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y);
    CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);
    CGContextSetLineWidth(context, 1.0);
    CGContextStrokePath(context);
    
    
    //Draw the eye
    CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
    CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[30]).x, NSPointToCGPoint (pt[30]).y, 40, 40));
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
    CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[30]).x+20, NSPointToCGPoint (pt[30]).y+5, 10, 10));

    //Draw Teeth
    //CGContextMoveToPoint(context,NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y);               
    //CGContextAddLineToPoint(context,NSPointToCGPoint(pt[27]).x, NSPointToCGPoint (pt[27]).y);
    //CGContextAddLineToPoint(context,NSPointToCGPoint(pt[28]).x, NSPointToCGPoint (pt[28]).y);
    //CGContextSetLineWidth(context, 1.0);
    //CGContextSetRGBStrokeColor(context, 255, 0.0, 0.0, 1.0); //Red
    //CGContextStrokePath(context);

    [self setNeedsDisplay:YES];
}

//---------------------------------------------------------------------------------

 - (void)drawRect:(NSRect)Rect
{
    CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	// Draw the background as a white square framed in black
	NSRect nsBounds = [self bounds];
	CGRect cgBounds = NSRectToCGRect(nsBounds);
	CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
	CGContextFillRect(context, cgBounds);
	CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
	CGContextSetLineWidth(context, 1.0);
	CGContextStrokeRect(context, cgBounds);
        
    switch (selectedSegment)
	{
		case 0:
                [self datatoscreen:nil];
                //Draw Basic Rest Position of Jaw pt array 0-6
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y); //C
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y);  //D
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[3]).x, NSPointToCGPoint (pt[3]).y);  //A
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y);  //D
                CGContextSetLineWidth(context, 1.0);
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 255); //Black
                CGContextStrokePath(context);
                //A2 muscle
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y);    //C
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
                CGContextSetLineWidth(context, 2.0);
                CGContextSetRGBStrokeColor(context, 255, 0.0, 0.0, 255); //Red
                CGContextStrokePath(context);
                //A3 muscle
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[5]).x, NSPointToCGPoint (pt[5]).y);    //I
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
                CGContextSetLineWidth(context, 2.0);
                CGContextSetRGBStrokeColor(context, 255, 0.0, 0.0, 1.0); //Red
                CGContextStrokePath(context);
                //Rotation points
                CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
                //CGContextSetRGBStrokeColor(context, 0.0, 255, 0.0, 255); //Green
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[3]).x, NSPointToCGPoint (pt[3]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[5]).x, NSPointToCGPoint (pt[5]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[18]).x, NSPointToCGPoint (pt[18]).y, 3, 3));
                CGContextStrokePath(context);
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            
                [self drawSkull:nil];
               
                newbins=bins;
                selectedSegment=0;
                graphiccheck=0;
                [self setNeedsDisplay:YES];
            break;
            
		case 1:         //Draw all the way open
            [self datatoscreen:nil];
            //draw open A2  
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[8]).x, NSPointToCGPoint (pt[8]).y); //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[7]).x, NSPointToCGPoint (pt[7]).y);  //D
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[9]).x, NSPointToCGPoint (pt[9]).y);  //A
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[7]).x, NSPointToCGPoint (pt[7]).y);  //D
            CGContextSetLineWidth(context, 1.0);
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            CGContextStrokePath(context);
            //A2 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[8]).x, NSPointToCGPoint (pt[8]).y);    //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //A3 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[10]).x, NSPointToCGPoint (pt[10]).y);    //I
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //Rotation points
            CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[7]).x, NSPointToCGPoint (pt[7]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[8]).x, NSPointToCGPoint (pt[8]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[9]).x, NSPointToCGPoint (pt[9]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[10]).x, NSPointToCGPoint (pt[10]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
             CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[19]).x, NSPointToCGPoint (pt[19]).y, 3, 3));
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            [self drawSkull:nil];

            //[self setNeedsDisplay:YES];
            newbins=0;
            selectedSegment=1;
            graphiccheck=0;
            [self setNeedsDisplay:YES];
            break;
            
        case 2:     //Open more (1 bin) button
            if (graphiccheck==0) {[self contless:nil];}
            [self datatoscreen:nil];
            //draw open A2  
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y); //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y);  //A
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
            CGContextSetLineWidth(context, 1.0);
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            CGContextStrokePath(context);
            //A2 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y);    //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //A3 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y);    //I
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //Rotation points
            CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
             CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[20]).x, NSPointToCGPoint (pt[20]).y, 3, 3));
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            
            [self drawSkull:nil];

            selectedSegment=2;
            graphiccheck=0;
            [self setNeedsDisplay:YES];
            break;
            
        case 3:     //Close more (1 bin) button
            if (graphiccheck==0) {[self contmore:nil];}
            [self datatoscreen:nil];
            //draw open A2  
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y); //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y);  //A
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
            CGContextSetLineWidth(context, 1.0);
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            CGContextStrokePath(context);
            //A2 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y);    //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //A3 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y);    //I
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //Rotation points
            CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
             CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[20]).x, NSPointToCGPoint (pt[20]).y, 3, 3));
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            
            [self drawSkull:nil];

            [self setNeedsDisplay:YES];
            selectedSegment=3;
            graphiccheck=0;
            break;
            
        case 4:     //animate all the way open and closed again
            newbins=bins;
            [self checkvars:nil];
            [self zerovars:nil];
            [self processdata1:nil];
            [self musclesim:nil];
            [self datatoscreen:nil];
            
            //Draw Basic Rest Position of Jaw pt array 0-6
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y); //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y);  //D
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[3]).x, NSPointToCGPoint (pt[3]).y);  //A
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y);  //D
            CGContextSetLineWidth(context, 1.0);
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            CGContextStrokePath(context);
            //A2 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y);    //C
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //A3 muscle
            CGContextMoveToPoint(context,NSPointToCGPoint(pt[5]).x, NSPointToCGPoint (pt[5]).y);    //I
            CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
            CGContextSetLineWidth(context, 2.0);
            CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
            CGContextStrokePath(context);
            //Rotation points
            CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[1]).x, NSPointToCGPoint (pt[1]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[2]).x, NSPointToCGPoint (pt[2]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[3]).x, NSPointToCGPoint (pt[3]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[5]).x, NSPointToCGPoint (pt[5]).y, 3, 3));
            CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
             CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[18]).x, NSPointToCGPoint (pt[18]).y, 3, 3));
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
            [self drawSkull:nil];

            NSDate *future = [NSDate dateWithTimeIntervalSinceNow: 0.1 ];
            [NSThread sleepUntilDate:future];
            
            [[NSGraphicsContext currentContext] flushGraphics];
            // Draw the background as a white square framed in black
            CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
            CGContextFillRect(context, cgBounds);
            CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
            CGContextSetLineWidth(context, 1.0);
            CGContextStrokeRect(context, cgBounds);
            
            int v=1;        //Open Animation
            while (v<bins){
                [self contless:nil];
                
                // Draw the background as a white square framed in black
                CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
                CGContextFillRect(context, cgBounds);
                CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
                CGContextSetLineWidth(context, 1.0);
                CGContextStrokeRect(context, cgBounds);
                //draw open A2  
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y); //C
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y);  //A
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
                CGContextSetLineWidth(context, 1.0);
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
                CGContextStrokePath(context);
                //A2 muscle
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y);    //C
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
                CGContextSetLineWidth(context, 2.0);
                CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
                CGContextStrokePath(context);
                //A3 muscle
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y);    //I
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
                CGContextSetLineWidth(context, 2.0);
                CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
                CGContextStrokePath(context);
                //Rotation points
                CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
                 CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[20]).x, NSPointToCGPoint (pt[20]).y, 3, 3));
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
                [self drawSkull:nil];

                [[NSGraphicsContext currentContext] flushGraphics];
                v=v+1;
                future = [NSDate dateWithTimeIntervalSinceNow: 0.1 ];
                [NSThread sleepUntilDate:future];
                [self datatoscreen:nil];
                [self setNeedsDisplay:YES];
            }         //End Open Animation
            v=1;      //Close Animation
            while (v<bins){
                [self contmore:nil];
                [self datatoscreen:nil];
                [self setNeedsDisplay:YES];
                // Draw the background as a white square framed in black
                CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
                CGContextFillRect(context, cgBounds);
                CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
                CGContextSetLineWidth(context, 1.0);
                CGContextStrokeRect(context, cgBounds);
                //draw open A2  
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);    //Q
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y); //C
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y);  //Q
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y);  //A
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y);  //D
                CGContextSetLineWidth(context, 1.0);
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
                CGContextStrokePath(context);
                //A2 muscle
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y);    //C
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y);  //A2
                CGContextSetLineWidth(context, 2.0);
                CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
                CGContextStrokePath(context);
                //A3 muscle
                CGContextMoveToPoint(context,NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y);    //I
                CGContextAddLineToPoint(context,NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y);  //A3
                CGContextSetLineWidth(context, 2.0);
                CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0); //Red
                CGContextStrokePath(context);
                //Rotation points
                CGContextSetRGBStrokeColor(context, 0, 0, 255, 1);//blue
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[0]).x, NSPointToCGPoint (pt[0]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[14]).x, NSPointToCGPoint (pt[14]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[15]).x, NSPointToCGPoint (pt[15]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[16]).x, NSPointToCGPoint (pt[16]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[4]).x, NSPointToCGPoint (pt[4]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[17]).x, NSPointToCGPoint (pt[17]).y, 3, 3));
                CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[6]).x, NSPointToCGPoint (pt[6]).y, 3, 3));
                 CGContextStrokeEllipseInRect(context, CGRectMake(NSPointToCGPoint(pt[20]).x, NSPointToCGPoint (pt[20]).y, 3, 3));
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); //Black
                [self drawSkull:nil];

                [[NSGraphicsContext currentContext] flushGraphics];
                v=v+1;
                future = [NSDate dateWithTimeIntervalSinceNow: 0.1 ];
                [NSThread sleepUntilDate:future];
                selectedSegment=0;
                graphiccheck=0;
            }         //End Close Animation
            
            break;
    }//end switch selected segment
    
}  // end drawrect


//---------------------------------------------------------------------------------
- (void)dealloc
{
    [super dealloc];
}

@end
