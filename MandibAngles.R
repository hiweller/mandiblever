
# computes distance b/w cartesian coordinates
# there's probably some R function that does this but it was faster to write it than hunt for it
distance <- function(p1, p2) {
  distance <- sqrt((p1[1] - p2[1])^2 + (p1[2] - p2[2])^2)
  return (as.vector(distance))
}

# returns a dataframe with 12 different lever lengths needed for calculations
ClosedMandibLengths <- function(arr) {
  LeverDF <- matrix(data=NA, ncol=12, nrow=length(arr[1,1,]))
  
  for (i in 1:length(arr[1,1,])){
    A2Inlever <- distance(arr[1,,i], arr[5,,i])
    A2Length <- distance(arr[4,,i], arr[5,,i])
    A2JawJoint <- distance(arr[4,,i], arr[1,,i])
    A3Inlever <- distance(arr[1,,i], arr[7,,i])
    A3Length <- distance(arr[6,,i], arr[7,,i])
    A3JawJoint <- distance(arr[6,,i], arr[1,,i])
    A3TendonLength <- distance(arr[7,,i], arr[8,,i])
    A2A3in <- distance(arr[5,,i], arr[7,,i])
    JawInlever <- distance(arr[1,,i], arr[3,,i])
    JawOutlever <- distance(arr[1,,i], arr[2,,i])
    TopJawLength <- distance(arr[5,,i], arr[2,,i])
    BottomJawLength <- distance(arr[3,,i], arr[2,,i])
    
    LeverVector <- c(A2Inlever, A2Length, A2JawJoint, A3Inlever,
                     A3Length, A3JawJoint, A3TendonLength, A2A3in, JawInlever, JawOutlever,
                     TopJawLength, BottomJawLength)
    
    LeverDF[i,] <- LeverVector
    
    LeverDF <- as.data.frame(LeverDF)
    colnames(LeverDF) = c("A2Inlever", "A2Length", "A2JawJoint",
                          "A3Inlever", "A3Length", "A3JawJoint",
                          "A3TendonLength", "A2A3in", "JawInlever",
                          "JawOutlever", "TopJawLength", "BottomJawLength")
  }
  return(LeverDF)
}

# I HAVE NO IDEA WHAT MARK IS DOING
MandibAngles <- function(dataframe) {
  for (i in 1:length(arr[1,1,])) {
    
    Triangle1 <- TriangleCalc(dataframe$A2Inlever[i], dataframe$A2Length[i], dataframe$A2JawJoint[i])
    Triangle2 <- TriangleCalc(dataframe$A2Inlever[i], dataframe$TopJawLength[i], dataframe$JawInlever[i])
    Triangle3 <- TriangleCalc(dataframe$A3Inlever[i], dataframe$A3Length[i], dataframe$A3JawJoint[i])
    Triangle4 <- TriangleCalc(dataframe$A3Inlever[i], dataframe$A2Inlever[i], dataframe$A2A3in[i])

  }
  
}

# creates 3d array of rotated points (points 4 & 6 are fixed)
Rotate <- function(dataframe, degrees) { # returns 3d array of rotated coordinates
  coords <- MandibCoords(dataframe)
  subset <- coords[c(1:3,5,7:8), , ]
  rads <- degrees*2*pi/360
  
  NewCoordArray <- array(data = NA, c(8,2,length(dataframe[,1])))
  
  RotationMatrix <- matrix(c(cos(rads), -sin(rads), sin(rads), cos(rads)), ncol=2, nrow=2)
  
  for (i in 1:length(dataframe[,1])) {
    NewCoords <- subset[,,i] %*% RotationMatrix
    NewCoordArray[1:3,,i] <- NewCoords[1:3,]
    NewCoordArray[4,,i] <- coords[4,,i]
    NewCoordArray[5,,i] <- NewCoords[4,]
    NewCoordArray[6,,i] <- coords[6,,i]
    NewCoordArray[7:8,,i] <- NewCoords[5:6,]
  }
  return(NewCoordArray)
}

# created 4d array of 3d arrays of rotated points
RotatedCoords <- function(dataframe, degreeRange=30, bins=20) {
  
  rads = degreeRange*2*pi/360
  AngleVector <- seq(from=0, to=rads, length=bins)
  
  BigStupidArray <- array(data=NA, c(8, 2, length(dataframe[,1]), bins))
  
  for (i in 1:bins) {
    OneArray <- Rotate(dataframe, -AngleVector[i])
    BigStupidArray[,,,i] <- OneArray
  }
  
  return(BigStupidArray)

}