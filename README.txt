@hiweller (Hannah Weller)
Date: April 10, 2015

MANDIBLEVER R PACKAGE 0.0.1

Found in "MandibLeveR.R" file (other code is supplementary/test files)
Use "Monotaxis_02.txt" as test file

MandibLever is an app created by Mark Westneat (Professor, OBA, Unversity of Chicago) used for simulating feeding mechanics in fishes
that computes bite force and mechanical advantage using a few simple coordinates and muscle masses. (Available here: 
https://westneatlab.uchicago.edu/page/biomechanics-software). The application is currently only Mac-compatible since it uses Xcode. 
The eventual goal of this project is to create a downloadable R package that produces the same results as MandibLever, but is compatible
 with any computer running a current version of R. As of 3/13/2015, this program is being used with R 3.0.2 "Frisbee Sailing".

MandibLever files are uploaded as a dataframe with 19 columns, one row per specimen, of the format "SpecID x1 y1 x2 y2...x8 y8 
musclemass1 musclemass2". The eight coordinates are described in the MandibLever manual (linked above), but are basically various
 points on the jaw or head indicating muscle insertions, origins, and inlevers/outlevers. The two muscle masses at the weights of two major 
subdivisions of the adductor mandibulae, which closes the jaw and is largely responsible for the production of bite force in jaw closing.


The "MandibPlot" function coerces a subset of the dataframe into a 3d array of (x, y) coordinates for each specimen (each slice of the 
array is an 8x2 matrix with one row per coordinate and x and y columns). The coordinates are rotated and translated using rotation and 
translation matrices so that the jaw joint (point number 1) is at the origin and the approximate midline of the jaw (between the jaw joint and 
the tip of the dentary bone, point 2) is parallel to the x-axis. These coordinates are then plotted separately and lines are drawn to simulate
 the lower jaw. Picture the output graphs of lines/triangles as the lower jaw with two muscles attached to it (blue lines = bone, red lines = 
muscle). 


The "BiteForceCalcs" function computes various lever lengths and angles between force input/output levers and calculates maximal bite 
force (at jaw closing). It also computes mechanical advantage (inlever/outlever ratio). 


The "MandibLeveR" function calls both of the above functions (so jaws are plotted and calculated at the same time). The results are saved
 in a csv file whose file path can be specified.


Next directions: 

+ the "RotatedCoords" function rotates the jaw (all but points 4 and 6, which are muscle attachment sites on the head and therefore fixed)
 over 30 degrees and 20 bins (angle of rotation and # of bins can be specified), which will eventually be used to calculate bite force and
 closing velocity at different points in jaw rotation

+ use R shiny package to create intuitive/easy-to-use user interface for changing variables or animating rotated jaw

+ calculate force output at different points in the jaw (rather than overall force output)

+ include optional muscle pennation variable 

+ compile into easily distributed R package